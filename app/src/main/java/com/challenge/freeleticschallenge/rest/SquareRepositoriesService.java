package com.challenge.freeleticschallenge.rest;

import com.challenge.freeleticschallenge.model.SquareRepositoryModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SquareRepositoriesService {

    @GET("/users/square/repos")
    Call<SquareRepositoryModel[]> getSquareRepositories();
}
