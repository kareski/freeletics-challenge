package com.challenge.freeleticschallenge.ui.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.challenge.freeleticschallenge.R;
import com.challenge.freeleticschallenge.ui.fragment.RepositoriesFragment;
import com.challenge.freeleticschallenge.ui.fragment.RepositoryDetailsFragment;

public class MainActivity extends AppCompatActivity
        implements RepositoriesFragment.OnSquareRepositorySelected {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showRepositoriesFragment();
    }

    @Override
    public void selectedRepository(int position) {

        RepositoryDetailsFragment repositoryDetailsFragment = (RepositoryDetailsFragment) getSupportFragmentManager()
                .findFragmentByTag(RepositoryDetailsFragment.TAG);

        int containerId = getResources().getBoolean(R.bool.isTablet)
                ? R.id.repository_details_fragment_container
                : R.id.repositories_fragment_container;

        // create the details fragment only the first time - reuse it after that
        // if the device is not a tablet -always replace the repositories fragment
        if (repositoryDetailsFragment == null || !getResources().getBoolean(R.bool.isTablet)) {
            repositoryDetailsFragment = new RepositoryDetailsFragment();

            Bundle bundle = new Bundle();
            bundle.putInt(RepositoryDetailsFragment.POSITION_ARG, position);
            repositoryDetailsFragment.setArguments(bundle);

            replaceFragmentInContainer(repositoryDetailsFragment, containerId,
                    RepositoryDetailsFragment.TAG);

            // when on tablet apply wrap_content constraints in order to give space to the details screen
            if (getResources().getBoolean(R.bool.isTablet)) {
                FrameLayout fl = findViewById(R.id.repositories_fragment_container);
                fl.setLayoutParams(new ConstraintLayout.LayoutParams(
                        ConstraintLayout.LayoutParams.WRAP_CONTENT,
                        ConstraintLayout.LayoutParams.MATCH_PARENT));
            }

        } else {
            repositoryDetailsFragment.updateSquareRepositoryInfo(position);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    /**
     * Displays the back button in the toolbar for the details scree (only on non-tablet devices)
     */
    public void showBackButton(boolean show) {
        if (!getResources().getBoolean(R.bool.isTablet)) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(show);
            }
        }
    }

    private void showRepositoriesFragment() {
        RepositoriesFragment repositoriesFragment =
                (RepositoriesFragment) getSupportFragmentManager()
                        .findFragmentByTag(RepositoriesFragment.TAG);

        if (repositoriesFragment == null) {
            repositoriesFragment = new RepositoriesFragment();
            replaceFragmentInContainer(repositoriesFragment, R.id.repositories_fragment_container,
                    RepositoriesFragment.TAG);
        }
        // in case of an activity rebuild we need to reassign the interface implementation
        repositoriesFragment.setOnSquareRepositorySelected(this);
    }

    private void replaceFragmentInContainer(Fragment fragment, @IdRes int containerId, String tag) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(containerId, fragment, tag);
        // if device is tablet there is no need to add to backstack -> we need onBackPressed() to exit the app
        // if not a tablet -> only if this is details fragment -> add to backstack
        if (!getResources().getBoolean(R.bool.isTablet) && tag.equals(RepositoryDetailsFragment.TAG)) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }
}
