package com.challenge.freeleticschallenge.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.challenge.freeleticschallenge.R;
import com.challenge.freeleticschallenge.adapter.SquareRepositoriesAdapter;
import com.challenge.freeleticschallenge.model.SquareRepositoryModel;
import com.challenge.freeleticschallenge.model.SquareRepositoryUiModel;
import com.challenge.freeleticschallenge.ui.activity.MainActivity;
import com.challenge.freeleticschallenge.viewmodel.SquareRepoViewModel;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class RepositoriesFragment extends Fragment {

    public static final String TAG = "RepositoriesFragment";

    private SquareRepositoriesAdapter squareRepositoriesAdapter;
    private OnSquareRepositorySelected onSquareRepositorySelected;
    private ProgressBar pgsBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_repositories, null);

        pgsBar = view.findViewById(R.id.progress_bar);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        squareRepositoriesAdapter = new SquareRepositoriesAdapter(new ArrayList<SquareRepositoryModel>(),
                onSquareRepositorySelected, getResources().getBoolean(R.bool.isTablet));
        recyclerView.setAdapter(squareRepositoriesAdapter);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((MainActivity) getActivity()).showBackButton(false);

        SquareRepoViewModel squareRepoViewModel = ViewModelProviders.of(requireActivity())
                .get(SquareRepoViewModel.class);
        squareRepoViewModel.getSquareRepoLiveData().observe(getViewLifecycleOwner(),
                new Observer<SquareRepositoryUiModel>() {
                    @Override
                    public void onChanged(SquareRepositoryUiModel squareRepositoryUiModel) {

                        switch (squareRepositoryUiModel.getStatus()) {
                            case SquareRepositoryUiModel.SUCCESS:
                                pgsBar.setVisibility(View.GONE);
                                squareRepositoriesAdapter.setSquareRepositories(squareRepositoryUiModel.getSquareRepositoryModels());
                                if(getResources().getBoolean(R.bool.isTablet)) {
                                    squareRepositoriesAdapter.selectFirstItem();
                                }
                                break;
                            case SquareRepositoryUiModel.LOADING:
                                pgsBar.setVisibility(View.VISIBLE);
                                break;
                            case SquareRepositoryUiModel.ERROR:
                                pgsBar.setVisibility(View.GONE);
                                Snackbar.make(pgsBar,
                                        squareRepositoryUiModel.getErrorMessage(),
                                        Snackbar.LENGTH_LONG).show();
                                break;
                            default:
                                pgsBar.setVisibility(View.GONE);
                                Snackbar.make(pgsBar,
                                        getString(R.string.something_unexpected),
                                        Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void setOnSquareRepositorySelected(OnSquareRepositorySelected onSquareRepositorySelected) {
        this.onSquareRepositorySelected = onSquareRepositorySelected;
    }

    /**
     * Interface for communication between the {@link RepositoriesFragment} and {@link MainActivity}
     * */
    public interface OnSquareRepositorySelected {
        void selectedRepository(int position);
    }
}
