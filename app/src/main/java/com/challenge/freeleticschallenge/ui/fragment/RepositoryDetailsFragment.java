package com.challenge.freeleticschallenge.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.challenge.freeleticschallenge.R;
import com.challenge.freeleticschallenge.model.SquareRepositoryModel;
import com.challenge.freeleticschallenge.model.SquareRepositoryUiModel;
import com.challenge.freeleticschallenge.ui.activity.MainActivity;
import com.challenge.freeleticschallenge.viewmodel.SquareRepoViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class RepositoryDetailsFragment extends Fragment {

    public static final String TAG = "RepositoryDetailsFragment";
    public static final String POSITION_ARG = "position_arg";

    private SquareRepoViewModel squareRepoViewModel;
    private TextView repoName;
    private TextView repoStars;
    private FloatingActionButton fab;
    private int selectedPosition = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_repository_details, null);

        setHasOptionsMenu(true);

        repoName = view.findViewById(R.id.nameTextView);
        repoStars = view.findViewById(R.id.starsTextView);
        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                squareRepoViewModel.toggleBookmark(repoName.getText().toString());
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((MainActivity) getActivity()).showBackButton(true);

        Bundle bundle = getArguments();
        if (bundle != null) {
            selectedPosition = bundle.getInt(POSITION_ARG);

            squareRepoViewModel = ViewModelProviders.of(requireActivity()).get(SquareRepoViewModel.class);
            squareRepoViewModel.getSquareRepoLiveData().observe(getViewLifecycleOwner(),
                    new Observer<SquareRepositoryUiModel>() {
                        @Override
                        public void onChanged(SquareRepositoryUiModel squareRepositoryUiModel) {
                            updateSquareRepositoryWithModel(squareRepositoryUiModel.getSquareRepositoryModels().get(selectedPosition));
                        }
                    });
        }
    }

    public void updateSquareRepositoryInfo(int position) {
        if (squareRepoViewModel != null
                && squareRepoViewModel.getSquareRepoLiveData() != null
                && squareRepoViewModel.getSquareRepoLiveData().getValue() != null) {
            selectedPosition = position;
            SquareRepositoryModel model = squareRepoViewModel.getSquareRepoLiveData().getValue().getSquareRepositoryModels().get(position);
            updateSquareRepositoryWithModel(model);
        }
    }

    private void updateSquareRepositoryWithModel(SquareRepositoryModel squareRepositoryModel) {
        repoName.setText(squareRepositoryModel.getName());
        repoStars.setText(String.format(getString(R.string.stargazers_), squareRepositoryModel.getStargazersCount()));
        fab.setImageResource(squareRepositoryModel.isBookmarked()
                ? android.R.drawable.star_big_on : android.R.drawable.star_big_off);
    }
}
