package com.challenge.freeleticschallenge.adapter;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.challenge.freeleticschallenge.R;
import com.challenge.freeleticschallenge.model.SquareRepositoryModel;
import com.challenge.freeleticschallenge.ui.fragment.RepositoriesFragment;

import java.util.ArrayList;
import java.util.List;

public class SquareRepositoriesAdapter extends
        RecyclerView.Adapter<SquareRepositoriesAdapter.SquareRepoViewHolder> {

    private final static int NO_SELECTED_POSITION = -1;

    private List<SquareRepositoryModel> squareRepositories;
    private RepositoriesFragment.OnSquareRepositorySelected onSquareRepositorySelected;
    private int selectedPosition = NO_SELECTED_POSITION;
    private boolean isTablet;

    public SquareRepositoriesAdapter(
            ArrayList<SquareRepositoryModel> squareRepositories,
            RepositoriesFragment.OnSquareRepositorySelected onSquareRepositorySelected,
            boolean isTablet) {
        this.squareRepositories = squareRepositories;
        this.onSquareRepositorySelected = onSquareRepositorySelected;
        this.isTablet = isTablet;
    }

    public void setSquareRepositories(List<SquareRepositoryModel> squareRepositories) {
        this.squareRepositories = squareRepositories;
        notifyDataSetChanged();
    }

    /**
     * Only used to preselect the 0-th element when on a tablet
     * */
    public void selectFirstItem() {
        if (selectedPosition == NO_SELECTED_POSITION) {
            selectedPosition = 0;
            onSquareRepositorySelected.selectedRepository(selectedPosition);
        }
    }

    @NonNull
    @Override
    public SquareRepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_square_repo,
                parent, false);

        return new SquareRepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SquareRepoViewHolder holder, int position) {
        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return squareRepositories.size();
    }

    class SquareRepoViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView starsTextView;

        SquareRepoViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.nameTextView);
            starsTextView = itemView.findViewById(R.id.starsTextView);
        }

        void bindData(final int position) {
            SquareRepositoryModel squareRepositoryModel = squareRepositories.get(position);

            if (isTablet) {
                if (selectedPosition == position) {
                    nameTextView.setTypeface(null, Typeface.BOLD);
                } else {
                    nameTextView.setTypeface(null, Typeface.NORMAL);
                }
            }

            nameTextView.setText(squareRepositoryModel.getName());
            nameTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0,
                    squareRepositoryModel.isBookmarked()
                            ? android.R.drawable.star_big_on
                            : android.R.drawable.star_big_off, 0);
            starsTextView.setText(String.valueOf(squareRepositoryModel.getStargazersCount()));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onSquareRepositorySelected.selectedRepository(position);
                    int oldPosition = selectedPosition;
                    selectedPosition = position;
                    notifyItemChanged(oldPosition);
                    notifyItemChanged(selectedPosition);
                }
            });
        }
    }
}