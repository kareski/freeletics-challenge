package com.challenge.freeleticschallenge.repository;

import androidx.lifecycle.MutableLiveData;

import com.challenge.freeleticschallenge.model.SquareRepositoriesResponse;
import com.challenge.freeleticschallenge.model.SquareRepositoryModel;
import com.challenge.freeleticschallenge.model.SquareRepositoryUiModel;
import com.challenge.freeleticschallenge.rest.SquareRepositoriesService;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SquareRepositories {
    private static final String BASE_URL = "https://api.github.com";

    private SquareRepositoriesService squareRepositoriesService;
    private final MutableLiveData<SquareRepositoriesResponse> squareRepoObservable;

    public SquareRepositories() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        squareRepositoriesService = retrofit.create(SquareRepositoriesService.class);

        squareRepoObservable = new MutableLiveData<>();
    }

    public MutableLiveData<SquareRepositoriesResponse> getSquareRepoObservable() {

        squareRepoObservable.setValue(
                new SquareRepositoriesResponse(null,
                        SquareRepositoryUiModel.LOADING, null));

        Call<SquareRepositoryModel[]> call = squareRepositoriesService.getSquareRepositories();
        call.enqueue(new Callback<SquareRepositoryModel[]>() {
            @Override
            public void onResponse(Call<SquareRepositoryModel[]> call,
                                   Response<SquareRepositoryModel[]> response) {

                List<SquareRepositoryModel> squareRepositoryModels = null;
                int responseStatus = SquareRepositoryUiModel.ERROR;
                if (response.isSuccessful()) {
                    squareRepositoryModels = response.body() != null ? Arrays.asList(response.body()) : null;
                    responseStatus = SquareRepositoryUiModel.SUCCESS;
                }
                squareRepoObservable.setValue(new SquareRepositoriesResponse(squareRepositoryModels,
                        responseStatus, null));
            }

            @Override
            public void onFailure(Call<SquareRepositoryModel[]> call, Throwable throwable) {
                squareRepoObservable.setValue(
                        new SquareRepositoriesResponse(null,
                                SquareRepositoryUiModel.ERROR, throwable.getMessage()));
            }
        });

        return squareRepoObservable;
    }

    /**
     * Re-sets the same value only to cause a refresh on the LiveData observer structure
     * */
    public void refreshViewModel() {
        squareRepoObservable.setValue(squareRepoObservable.getValue());
    }
}