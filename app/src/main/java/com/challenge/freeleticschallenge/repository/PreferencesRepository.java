package com.challenge.freeleticschallenge.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashSet;
import java.util.Set;

public class PreferencesRepository {

    private static final String BOOKMARKED_REPOSITORIES_PREFERENCE = "bookmarked_repository_preference";

    private SharedPreferences sharedPreferences;
    /**
     * bookmarksSet is a cached copy of the bookmark preferences
     * */
    private Set<String> bookmarksSet;

    public PreferencesRepository(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        bookmarksSet = new HashSet<>(
                sharedPreferences.getStringSet(BOOKMARKED_REPOSITORIES_PREFERENCE, new HashSet<String>()));
    }

    public boolean isRepositoryBookmarked(String repositoryName) {
        if (bookmarksSet != null) {
            return bookmarksSet.contains(repositoryName);
        }

        return false;
    }

    public void toggleBookmarkRepository(String repositoryName) {
        if (bookmarksSet == null) {
            bookmarksSet = new HashSet<>();
        }

        if (bookmarksSet.contains(repositoryName)) {
            bookmarksSet.remove(repositoryName);
        } else {
            bookmarksSet.add(repositoryName);
        }

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putStringSet(BOOKMARKED_REPOSITORIES_PREFERENCE, bookmarksSet)
                .apply();
    }
}
