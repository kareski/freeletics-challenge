package com.challenge.freeleticschallenge.model;

import androidx.annotation.Nullable;

import java.util.List;

public class SquareRepositoriesResponse {

    private final List<SquareRepositoryModel> squareRepositories;
    private final int status;
    private final String errorMessage;

    public SquareRepositoriesResponse(@Nullable List<SquareRepositoryModel> squareRepositories,
                                      @SquareRepositoryUiModel.Status int status, String errorMessage) {
        this.squareRepositories = squareRepositories;
        this.status = status;
        this.errorMessage = errorMessage;
    }

    public List<SquareRepositoryModel> getSquareRepositories() {
        return squareRepositories;
    }


    public @SquareRepositoryUiModel.Status int getStatus() {
        return status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
