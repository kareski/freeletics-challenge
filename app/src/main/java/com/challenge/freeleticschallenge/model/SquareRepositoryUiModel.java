package com.challenge.freeleticschallenge.model;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public class SquareRepositoryUiModel {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({SUCCESS, ERROR, LOADING})
    @interface Status {}

    public static final int SUCCESS = 0;
    public static final int ERROR = 1;
    public static final int LOADING = -1;

    private final List<SquareRepositoryModel> squareRepositoryModels;
    private final @Status int status;
    private final String errorMessage;

    public SquareRepositoryUiModel(List<SquareRepositoryModel> squareRepositoryModels,
                                   @Status int status, String errorMessage) {
        this.squareRepositoryModels = squareRepositoryModels;
        this.status = status;
        this.errorMessage = errorMessage;
    }

    public List<SquareRepositoryModel> getSquareRepositoryModels() {
        return squareRepositoryModels;
    }

    public @Status int getStatus() {
        return status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}