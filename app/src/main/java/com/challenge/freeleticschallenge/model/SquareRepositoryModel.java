package com.challenge.freeleticschallenge.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class SquareRepositoryModel {
    @SerializedName("name")
    private String name;

    @SerializedName("stargazers_count")
    private int stargazersCount;

    private boolean isBookmarked;

    public String getName() {
        return name;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }

    public boolean isBookmarked() {
        return isBookmarked;
    }

    public void setBookmarked(boolean isBookmarked) {
        this.isBookmarked = isBookmarked;
    }

    @NonNull
    @Override
    public String toString() {
        return getName() + " " + getStargazersCount() + " " + isBookmarked();
    }
}
