package com.challenge.freeleticschallenge.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.challenge.freeleticschallenge.model.SquareRepositoriesResponse;
import com.challenge.freeleticschallenge.model.SquareRepositoryModel;
import com.challenge.freeleticschallenge.model.SquareRepositoryUiModel;
import com.challenge.freeleticschallenge.repository.PreferencesRepository;
import com.challenge.freeleticschallenge.repository.SquareRepositories;

import java.util.List;

public class SquareRepoViewModel extends AndroidViewModel {

    private final LiveData<SquareRepositoryUiModel> squareRepoLiveData;
    private final SquareRepositories repository;
    private final PreferencesRepository preferencesRepository;

    public SquareRepoViewModel(@NonNull Application application) {
        super(application);

        repository = new SquareRepositories();
        preferencesRepository = new PreferencesRepository(application.getApplicationContext());

        squareRepoLiveData = Transformations.map(
                repository.getSquareRepoObservable(),
                new Function<SquareRepositoriesResponse, SquareRepositoryUiModel>() {

                    @Override
                    public SquareRepositoryUiModel apply(SquareRepositoriesResponse response) {
                        List<SquareRepositoryModel> squareRepoViewModels = updateBookmarks(response.getSquareRepositories());
                        return new SquareRepositoryUiModel(
                                squareRepoViewModels,
                                response.getStatus(),
                                response.getErrorMessage());
                    }
                });
    }

    private List<SquareRepositoryModel> updateBookmarks(List<SquareRepositoryModel> squareRepositoryModels) {
        if (squareRepositoryModels != null) {
            for (int i = 0; i < squareRepositoryModels.size(); i++) {
                SquareRepositoryModel model = squareRepositoryModels.get(i);
                if (preferencesRepository.isRepositoryBookmarked(model.getName())) {
                    model.setBookmarked(true);
                } else {
                    model.setBookmarked(false);
                }
            }
        }

        return squareRepositoryModels;
    }

    public LiveData<SquareRepositoryUiModel> getSquareRepoLiveData() {
        return squareRepoLiveData;
    }

    public void toggleBookmark(String repositoryName) {
        preferencesRepository.toggleBookmarkRepository(repositoryName);
        if (squareRepoLiveData.getValue() != null) {
            repository.refreshViewModel();
        }
    }
}
