The solution uses one activity - two fragments architecture with a shared ViewModel.

The repositories fragment fetches and displayes the list of repositories using the LiveData's observer pattern. It informs the repository details fragment only of the selected position. The repository details fragment uses the same ViewModel with the same LiveData objects to manipulate the bookmark state of a repository.

The bookmarks are stored in SharedPreference on each modification but they are fetched from the SharedPreference only once, in the constructor of PreferencesRepository.java.

The usage of fragments and layouts also takes into consideration the size of the device, so if the device is a tablet a typical master-detail page will be presented.